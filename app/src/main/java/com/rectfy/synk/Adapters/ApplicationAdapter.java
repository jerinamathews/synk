package com.rectfy.synk.Adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff.Mode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.rectfy.synk.Models.ApplicationInfoToShow;
import com.rectfy.synk.R;

import java.util.List;

public class ApplicationAdapter extends ArrayAdapter<ApplicationInfoToShow> {
    static final /* synthetic */ boolean $assertionsDisabled = (!ApplicationAdapter.class.desiredAssertionStatus());
    private List<ApplicationInfoToShow> appsList;
    private Context context;
    private PackageManager packageManager;

    private static class appsInfo {
        ImageView appsIcon;
        TextView appsName;
        ImageView checkBox;

        private appsInfo() {
        }
    }

    public ApplicationAdapter(Context context, int i, List<ApplicationInfoToShow> arrayList) {
        super(context, i, arrayList);
        this.appsList = arrayList;
        this.context = context;
        this.packageManager = context.getPackageManager();
    }

    public int getCount() {
        return this.appsList != null ? this.appsList.size() : 0;
    }

    public ApplicationInfoToShow getItem(int position) {
        return this.appsList != null ? (ApplicationInfoToShow) this.appsList.get(position) : null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        appsInfo aVar;
        if (view != null) {
            aVar = (appsInfo) view.getTag();
        } else if ($assertionsDisabled || this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) != null) {
            view = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_app_layout, null);
            aVar = new appsInfo();
            aVar.appsName = (TextView) view.findViewById(R.id.row_app_layout_tv_name);
            aVar.appsIcon = (ImageView) view.findViewById(R.id.row_app_layout_iv_icon);
            aVar.checkBox = (ImageView) view.findViewById(R.id.row_app_layout_iv_cb);
            view.setTag(aVar);
        } else {
            throw new AssertionError();
        }
        ApplicationInfoToShow applicationInfoToShow = (ApplicationInfoToShow) this.appsList.get(i);
        aVar.appsName.setText(applicationInfoToShow.getName());
        aVar.appsIcon.setImageDrawable(applicationInfoToShow.getIcon());
        if (applicationInfoToShow.isSelected()) {
            aVar.checkBox.setImageResource(R.drawable.ic_check_box_white_24dp);
        } else {
            aVar.checkBox.setImageResource(R.drawable.ic_check_box_outline_blank_white_24dp);
        }
        aVar.checkBox.getDrawable().setColorFilter(ContextCompat.getColor(this.context, R.color.light_icon_2), Mode.MULTIPLY);
        return view;
    }
}
