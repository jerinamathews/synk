package com.rectfy.synk;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

public class PhonecallReceiver extends BroadcastReceiver {

    static Boolean ringing = false;
    static Boolean call_received=false;
    private Socket m3Socket;



    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
            try {

                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    ringing = true;
                    call_received = false;
                    m3Socket = ForegroundService.mSocket;
                    if(m3Socket == null ){
                        return;
                    }
                    String name = getContactName(incomingNumber, context);
                    if(incomingNumber != null && incomingNumber.length() >0) {

                        JSONObject data = new JSONObject();
                        try {
                            data.put("type", "incomingCall");
                            data.put("name", name);
                            data.put("number", incomingNumber);
                            m3Socket.emit("serverEventAndroid", data.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
                if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.e("Call Alert", "Call off hook");
                    call_received = true;
                }
                if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.e("Call Alert", "Call idle");
                    Log.e("Call Alert", ringing+" "+call_received);
                        if (ringing && !call_received) {
                            Log.e("Call Alert", "Missed Call");
                            String name = getContactName(incomingNumber, context);
                            m3Socket = ForegroundService.mSocket;
                            if(m3Socket == null ){
                                return;
                            }
                            JSONObject data1 = new JSONObject();
                            try {
                                data1.put("type", "missedCall");
                                data1.put("name", name);
                                data1.put("number", incomingNumber);
                                m3Socket.emit("serverEventAndroid", data1.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    call_received = ringing = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }



    }
    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }




}