package com.rectfy.synk.Models;

public class ApplicationInfo {
    private String name;
    private String packageName;
    private boolean selected;

    public ApplicationInfo(String name, String packageName) {
        this.name = name;
        this.packageName = packageName;
        this.selected = false;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean b) {
        this.selected = b;
    }

    public boolean equals(Object obj) {
        if (this.packageName.equals(((ApplicationInfo) obj).getPackageName()) && this.name.equals(((ApplicationInfo) obj).getName())) {
            return true;
        }
        return false;
    }

    public ApplicationInfoToShow toApplicationInfoToShow() {
        ApplicationInfoToShow applicationInfoToShow = new ApplicationInfoToShow(this.name, this.packageName);
        applicationInfoToShow.setIcon(null);
        applicationInfoToShow.setSelected(isSelected());
        return applicationInfoToShow;
    }
}
