package com.rectfy.synk.Models;

import android.graphics.drawable.Drawable;

public class ApplicationInfoToShow {
    private Drawable icon;
    private String name;
    private String packageName;
    private boolean selected;

    public ApplicationInfoToShow(String name, String packageName) {
        this.name = name;
        this.packageName = packageName;
        this.selected = false;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setIcon(Drawable drawable) {
        this.icon = drawable;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean b) {
        this.selected = b;
    }

    public boolean equals(Object obj) {
        if (this.packageName.equals(((ApplicationInfoToShow) obj).getPackageName()) && this.name.equals(((ApplicationInfoToShow) obj).getName())) {
            return true;
        }
        return false;
    }

    public ApplicationInfo toApplicationInfo() {
        ApplicationInfo applicationInfo = new ApplicationInfo(this.name, this.packageName);
        applicationInfo.setSelected(isSelected());
        return applicationInfo;
    }

    public ApplicationInfoToShow() {
    }
}
