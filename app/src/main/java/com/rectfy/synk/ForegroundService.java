package com.rectfy.synk;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class ForegroundService extends Service {
    public static final String CHANNEL_ID = "SynkService";
    private ClipboardManager clipboard;

    private String clipMessage="";

    static Socket mSocket;


    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Synk")
                .setContentText("Synk foreground")
                .setSmallIcon(R.drawable.ic_apps)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        //do heavy work on a background thread


        String URL = getSharedPreferences("PREF", MODE_PRIVATE).getString("SOCKET_URL",null);
        if(URL != null ) {

            try {
                mSocket = IO.socket(URL);
            } catch (URISyntaxException ignored) {
            }

            mSocket.connect();

            mSocket.on("clientEventAndroid", onNewMessage);


            clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboard != null) {
                clipboard.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
                    @Override
                    public void onPrimaryClipChanged() {
                        if (clipboard.hasPrimaryClip()) {
                            if(clipboard.getPrimaryClip().getItemAt(0).getText() != null){
                            String message = clipboard.getPrimaryClip().getItemAt(0).getText().toString();

                            if (!clipMessage.equals(message)) {
                                clipMessage = message;
                                JSONObject clipData = new JSONObject();
                                try {
                                    clipData.put("type", "clipboard");
                                    clipData.put("content", message);
                                    mSocket.emit("serverEventAndroid", clipData.toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            }
                            else{
                                Log.e("clip", clipboard.getPrimaryClip().getItemAt(0).getHtmlText());
                            }

                        }


                    }
                });
            }
        }
        else{
            Toast.makeText(this, "NO URL", Toast.LENGTH_SHORT).show();
            stopForeground(true);
            stopSelf();
            return START_NOT_STICKY;
        }


        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        if(mSocket!= null && mSocket.connected())
            mSocket.disconnect();
        super.onDestroy();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Synk",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            clipMessage = args[0].toString();
            Log.e("data", clipMessage);
            try {
                JSONObject json = new JSONObject(clipMessage);
                if(json.getString("type").equals("clipboard")){
                    ClipData clip = ClipData.newPlainText("label", json.getString("content"));
                    clipboard.setPrimaryClip(clip);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    };
}