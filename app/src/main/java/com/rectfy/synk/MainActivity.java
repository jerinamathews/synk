package com.rectfy.synk;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Button btnStartStopService;
    private static final int REQUEST = 10;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 23) {
            checkAndRequestPermissions();
        }

        btnStartStopService = findViewById(R.id.buttonStartService);
        btnStartStopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStopService();
            }
        });


        String URL = getSharedPreferences("PREF", MODE_PRIVATE).getString("SOCKET_URL",null);
        if(URL != null ) {
            ((EditText)(findViewById(R.id.socketUrl))).setText(URL);
        }

        if(ForegroundService.mSocket != null){
            btnStartStopService.setText("STOP");
        }


    }

    public void startStopService() {
        switch (btnStartStopService.getText().toString()){
            case "START":
                startService();
                break;
            case "STOP":
                stopService();
                break;
        }
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, serviceIntent);

        btnStartStopService.setText("STOP");
    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        stopService(serviceIntent);

        btnStartStopService.setText("START");
        if(ForegroundService.mSocket != null){
            ForegroundService.mSocket.disconnect();
        }
    }

    public void clearText(View view) {
        ((EditText)findViewById(R.id.copy_content)).setText("");
    }

    private void checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            int phonepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            int callLogpermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG);

            int contactpermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);


            List<String> listPermissionsNeeded = new ArrayList<>();

            if (phonepermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (callLogpermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_CALL_LOG);
            }
            if (contactpermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && requestCode==REQUEST) {
            Map<String, Integer> perms = new HashMap<>();
            // Initialize the map with both permissions
            perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for both permissions
            if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//                        Log.d(TAG, "sms & location services permission granted");
                // process the normal flow
                boolean perm = true;
                //else any one or both the permissions are not granted
            } else {
                Log.d("CallAlert", "Some permissions are not granted ask again ");
                //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Phone and Contact Permissions are required for this app, since this app provides alert when a call arrives.", Toast.LENGTH_SHORT).show();

                }
                //permission is denied (and never ask again is  checked)
                //shouldShowRequestPermissionRationale will return false
                else {
                    Toast.makeText(this, "You need to give Phone and Contact permission to continue. Do you want to go to app settings?", Toast.LENGTH_SHORT).show();
                    //                            //proceed with logic by disabling the related features or quit the app.
                }
            }
        }
    }

    public void saveSocketUrl(View view) {
        String url = ((EditText)(findViewById(R.id.socketUrl))).getText().toString();
        if(url.length() > 0){
            getSharedPreferences("PREF", MODE_PRIVATE).edit().putString("SOCKET_URL", url).apply();
            Toast.makeText(this, "URL saved", Toast.LENGTH_SHORT).show();
        }
    }

    public void openManageApps(View view) {
        startActivity(new Intent(MainActivity.this, ManageAppActivity.class));
    }
}