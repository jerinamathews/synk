package com.rectfy.synk.service;

import java.util.HashSet;
import java.util.Set;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.FileObserver;
import android.os.IBinder;
import android.util.Log;

import com.rectfy.synk.R;

/**
 * Starts a background thread to monitor the states of clipboard and stores
 * any new clips into the SQLite database.
 * <p>
 * <i>Note:</i> the current android clipboard system service only supports
 * text clips, so in browser, we can just save images to external storage
 * (SD card). This service also monitors the downloads of browser, if any
 * image is detected, it will be stored into SQLite database, too.   
 */
public class ClipboardMonitor extends Service {


    private MonitorTask mTask = new MonitorTask();
    private ClipboardManager mCM;

    
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mCM = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        mTask.start();
    }


    
    @Override
    public void onDestroy() {
        mTask.cancel();
    }
    
    @Override
    public void onStart(Intent intent, int startId) {
    }

    /**
     * Monitor task: monitor new text clips in global system clipboard and
     * new image clips in browser download directory
     */
    private class MonitorTask extends Thread {

        private volatile boolean mKeepRunning = false;
        private String mOldClip = null;

        public MonitorTask() {
            super("ClipboardMonitor");
        }

        /** Cancel task */
        public void cancel() {
            mKeepRunning = false;
            interrupt();
        }
        
        @Override
        public void run() {
            mKeepRunning = true;
            while (true) {
                doTask();
                if (!mKeepRunning) {
                    break;
                }
            }
        }
        
        private void doTask() {
            if (mCM.hasPrimaryClip()) {
                ClipData newClip = mCM.getPrimaryClip();

                if (!newClip.getItemAt(0).getText().toString().equals(mOldClip)) {
                    mOldClip = newClip.getItemAt(0).getText().toString();
                    String TAG = "asasa";
                    Log.i(TAG, "new text clip inserted: " + newClip.toString());
                }
            }
        }

    }
}
