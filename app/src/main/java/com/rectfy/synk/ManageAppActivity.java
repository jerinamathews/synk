package com.rectfy.synk;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rectfy.synk.Adapters.ApplicationAdapter;
import com.rectfy.synk.Models.ApplicationInfo;
import com.rectfy.synk.Models.ApplicationInfoToShow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManageAppActivity extends AppCompatActivity {
    private static SharedPreferences sharedPreferences;
    private ApplicationAdapter applicationAdapter;
    private List<ApplicationInfo> arrAppList = new ArrayList<>();
    private List<ApplicationInfoToShow> arrAppListToShow = new ArrayList<>();
    private SharedPreferences.Editor editor;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        final ManageAppActivity manageAppActivity = ManageAppActivity.this;

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.manage_app_active_container:
                    this.manageAppActivity.openNotificationListenerSettings();
                    return;
                case R.id.manage_app_title_container_up:
                    this.manageAppActivity.finish();
                    return;
                default:
            }
        }
    };
    private TextView tvCount;
    private TextView tvStatus;
    private TextView tvTitle;

    @SuppressLint({"StaticFieldLeak"})
    private class LoadApplications extends AsyncTask<Object, ApplicationInfoToShow, Void> {
        final ManageAppActivity manageAppActivity;

        protected Void doInBackground(Object... objArr) {
            return doInBackground();
        }

        protected void onPostExecute(Void obj) {
            onPost(obj);
        }

        protected void onProgressUpdate(ApplicationInfoToShow... objArr) {
            update(objArr);
        }

        private LoadApplications(ManageAppActivity manageAppActivity) {
            this.manageAppActivity = manageAppActivity;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        Void doInBackground() {
            List arrayList;
            String string = getSharedPreferences("PREF",MODE_PRIVATE).getString("APPS", null);
            if (string != null) {
                this.manageAppActivity.arrAppList = new Gson().fromJson(string, new TypeToken<ArrayList<ApplicationInfo>>() {
                    final LoadApplications loadApplications = LoadApplications.this;
                }.getType());
                arrayList = arrAppList;
                for (Object anArrayList : arrayList) {
                    ApplicationInfo applicationInfo = (ApplicationInfo) anArrayList;
                    if (!this.manageAppActivity.havePackageName(applicationInfo.getPackageName())) {
                        this.manageAppActivity.arrAppList.remove(applicationInfo);
                    }
                }
                if (this.manageAppActivity.arrAppList.isEmpty()) {
                    getSharedPreferences("PREF",MODE_PRIVATE).edit().putString("APPS", null).apply();
                } else {
                    getSharedPreferences("PREF",MODE_PRIVATE).edit().putString("APPS", new Gson().toJson(this.manageAppActivity.arrAppList)).apply();
                }
            }
            PackageManager packageManager = this.manageAppActivity.getApplicationContext().getPackageManager();
            Intent intent = new Intent("android.intent.action.MAIN", null);
            intent.addCategory("android.intent.category.LAUNCHER");
            arrayList = (ArrayList) this.manageAppActivity.getApplicationContext().getPackageManager().queryIntentActivities(intent, 0);
            this.manageAppActivity.arrAppListToShow.clear();
            for (Object anArrayList : arrayList) {
                ResolveInfo resolveInfo = (ResolveInfo) anArrayList;
                ApplicationInfoToShow applicationInfoToShow = new ApplicationInfoToShow();
                applicationInfoToShow.setName(resolveInfo.loadLabel(packageManager).toString());
                applicationInfoToShow.setIcon(resolveInfo.loadIcon(packageManager));
                applicationInfoToShow.setPackageName(resolveInfo.activityInfo.packageName);
                publishProgress(applicationInfoToShow);
            }
            return null;
        }

        void update(ApplicationInfoToShow... applicationInfoToShowArr) {
            super.onProgressUpdate(applicationInfoToShowArr);
            ApplicationInfoToShow applicationInfoToShow = applicationInfoToShowArr[0];
            this.manageAppActivity.countApp();
            if (this.manageAppActivity.arrAppList.contains(applicationInfoToShow.toApplicationInfo())) {
                applicationInfoToShow.setSelected(true);
                if (arrAppListToShow.isEmpty()) {
                    arrAppListToShow.add(0, applicationInfoToShow);
                } else {
                    List<ApplicationInfoToShow> arrayList = arrAppListToShow;
                    int i = 0;
                    while (i < arrayList.size()) {
                        if (!((ApplicationInfoToShow) arrayList.get(i)).isSelected()) {
                            this.manageAppActivity.arrAppListToShow.add(i, applicationInfoToShow);
                            break;
                        }
                        i++;
                    }
                    if (i == arrayList.size()) {
                        this.manageAppActivity.arrAppListToShow.add(applicationInfoToShow);
                    }
                }
            } else {
                this.manageAppActivity.arrAppListToShow.add(applicationInfoToShow);
            }
            this.manageAppActivity.applicationAdapter.notifyDataSetChanged();
        }

        void onPost(Void voidR) {
            super.onPostExecute(voidR);
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_manage_app);
        findViewById(R.id.manage_app_title_container_up).setOnClickListener(this.onClickListener);
        findViewById(R.id.manage_app_active_container).setOnClickListener(this.onClickListener);
        this.tvCount = findViewById(R.id.tvCount);
        this.tvTitle = findViewById(R.id.manage_app_active_text_title);
        this.tvStatus = findViewById(R.id.manage_app_active_text_status);
        ListView lvApp = findViewById(R.id.manage_app_listview);
        this.applicationAdapter = new ApplicationAdapter(this, 0, this.arrAppListToShow);
        lvApp.setAdapter(this.applicationAdapter);
        new LoadApplications(this).execute(new Void[0]);
        lvApp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            final ManageAppActivity manageAppActivity = ManageAppActivity.this;

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Iterator iterator;
                int count = 0;
                if (this.manageAppActivity.arrAppListToShow.get(i).isSelected()) {
                    this.manageAppActivity.arrAppListToShow.get(i).setSelected(false);
                } else {
                    iterator = this.manageAppActivity.arrAppListToShow.iterator();
                    while (iterator.hasNext()) {
                        if (((ApplicationInfoToShow) iterator.next()).isSelected()) {
                            count++;
                        }
                    }
                    this.manageAppActivity.arrAppListToShow.get(i).setSelected(true);
                }
                this.manageAppActivity.applicationAdapter.notifyDataSetChanged();
                this.manageAppActivity.arrAppList.clear();
                iterator = this.manageAppActivity.arrAppListToShow.iterator();
                while (iterator.hasNext()) {
                    ApplicationInfoToShow applicationInfoToShow = (ApplicationInfoToShow) iterator.next();
                    if (applicationInfoToShow.isSelected()) {
                        this.manageAppActivity.arrAppList.add(applicationInfoToShow.toApplicationInfo());
                    }
                }
                this.manageAppActivity.countApp();
                if (this.manageAppActivity.arrAppList.isEmpty()) {
                    getSharedPreferences("PREF",MODE_PRIVATE).edit().putString("APPS", null).apply();
                    return;
                }
                getSharedPreferences("PREF",MODE_PRIVATE).edit().putString("APPS", new Gson().toJson(this.manageAppActivity.arrAppList)).apply();
            }
        });

    }

    private void askNotificationAccess(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCanceledOnTouchOutside(false);
        try {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception ignored){}
        final Button yes = dialog.findViewById(R.id.dialog_yes);
        final Button no = dialog.findViewById(R.id.dialog_no);
        final TextView content = dialog.findViewById(R.id.dialog_heading);
        content.setText("We need notification access permission to detect incoming notification and alert you through flash. Grant us the permission now");
        yes.setText("Give Access");
        no.setText("Cancel");

        yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openNotificationListenerSettings();
                dialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    protected void onResume() {
        if (NotificationService.isActive) {
            this.tvTitle.setText(getString(R.string.tab_to_turn_off));
            this.tvTitle.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            this.tvStatus.setText("ON");
            this.tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.status_on));
        } else {
            askNotificationAccess();
            this.tvTitle.setText(getString(R.string.tab_to_turn_on));
            this.tvTitle.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            this.tvStatus.setText("OFF");
            this.tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.status_off));
        }
        super.onResume();
    }

    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private boolean havePackageName(String str) {
        try {
            getPackageManager().getPackageInfo(str, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void openNotificationListenerSettings() {
        Intent intent;
        try {
            intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
        } catch (Exception e) {
            intent = new Intent("android.settings.ACCESSIBILITY_SETTINGS");
        }
        startActivityForResult(intent, 0);
    }

    private void countApp() {
        if (this.tvCount != null && this.arrAppList != null) {
            this.tvCount.setText(this.arrAppList.size() > 0 ? String.valueOf(this.arrAppList.size()) : "");
        }
    }
}
