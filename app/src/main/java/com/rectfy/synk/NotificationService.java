package com.rectfy.synk;


import android.annotation.TargetApi;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

@TargetApi(18)
public class NotificationService extends NotificationListenerService {
    public static boolean isActive;
    private String TAG = getClass().getSimpleName();
    public ArrayList<ApplicationInfo> appList;
    private Context context;
    private boolean isAppSelected = false;

    class NLServiceReceiver extends BroadcastReceiver {
        NLServiceReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
        }
    }

    public void onCreate() {
        super.onCreate();
        this.context = getApplicationContext();
        isActive = true;
    }

    public void onNotificationPosted(StatusBarNotification sbn) {

        Type type = new TypeToken<ArrayList<ApplicationInfo>>() {}.getType();
        String string = getSharedPreferences("PREF", MODE_PRIVATE).getString("APPS", null);
        if (string != null) {
            this.appList = new Gson().fromJson(string, type);
        }

        String appName = "";
        this.isAppSelected = false;

        if(this.appList.size() > 0) {

            Iterator it = this.appList.iterator();
            while (it.hasNext()) {
                ApplicationInfo appObj = (ApplicationInfo) it.next();
                if (appObj.packageName.equals(getStatusPackageName(sbn))) {
                    this.isAppSelected = true;
                    appName = appObj.name;
                    break;
                }
            }
        }

        if(this.isAppSelected){
            String title = sbn.getNotification().extras.getString("android.title");
            String text = "", text2="", text3 ="", message="Unreadable";

            if (sbn.getNotification().extras.getString("android.text") != null)
                text = sbn.getNotification().extras.getString("android.text");

            if (sbn.getNotification().extras.getCharSequenceArray("android.textLines") != null)
                text2 = Arrays.toString(sbn.getNotification().extras.getCharSequenceArray("android.textLines"));


            Bundle extras = sbn.getNotification().extras;
            if (extras.get(Notification.EXTRA_BIG_TEXT) != null) {
                text3 = extras.get(Notification.EXTRA_BIG_TEXT).toString();
            }

            if(text.length()>0){
                message = text;
            }
            else if(text2.length()>0){
                message = text2;
            }
            else if(text3.length()>0){
                message = text3;
            }
            if(ForegroundService.mSocket != null) {
                JSONObject notif = new JSONObject();
                try {
                    notif.put("type", "notification");
                    notif.put("title", title);
                    notif.put("content", message);
                    notif.put("app", appName);
                    ForegroundService.mSocket.emit("serverEventAndroid", notif.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                Log.e("app", appName);
            }
        }

    }

    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.e("notification removed", getStatusPackageName(sbn));

    }

    public IBinder onBind(Intent intent) {
        isActive = true;
        Type type = new TypeToken<ArrayList<ApplicationInfo>>() {}.getType();
        String string = getSharedPreferences("PREF", MODE_PRIVATE).getString("APPS", null);
        if (string != null) {
            this.appList = new Gson().fromJson(string, type);
        }
        return super.onBind(intent);
    }

    public void onDestroy() {
        isActive = false;
        super.onDestroy();
    }




    @TargetApi(18)
    private String getStatusPackageName(StatusBarNotification sbn) {
        return sbn.getPackageName();
    }
}
